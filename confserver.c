/*--------------------------------------------------------------------*/
/* conference server */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netdb.h>
#include <time.h> 
#include <errno.h>
#include <stdlib.h>

extern char * recvtext(int sd);
extern int sendtext(int sd, char *msg);

extern int startserver();
/*--------------------------------------------------------------------*/

/*--------------------------------------------------------------------*/
int fd_isset(int fd, fd_set *fsp) {
	return FD_ISSET(fd, fsp);
}
/* main routine */
int main(int argc, char *argv[]) {
	int servsock; /* server socket descriptor */
	socklen_t addr_size;
	fd_set livesdset, sec; /* set of live client sockets */
	int livesdmax; /* largest live client socket descriptor */
	struct sockaddr_in serveraddr;  
	struct sockaddr_in clientaddr;
	struct hostent *hp;

	/* check usage */
	if (argc != 1) {
		fprintf(stderr, "usage : %s\n", argv[0]);
		exit(1);
	}
	/* get ready to receive requests */
	servsock = startserver();
	if (servsock == -1) {
		perror("Error on starting server: ");
		exit(1);
	}
	/*
	 FILL HERE:			X
	 init the set of live clients
	 */
	FD_ZERO(&livesdset);
	FD_SET(servsock, &livesdset);	
  livesdmax = servsock;	
	/* receive requests and process them */
	while (1) {
		int frsock; /* loop variable */

		/*
		 FILL HERE		X
		 wait using select() for
		 messages from existing clients and
		 connect requests from new clients
		 */
		sec = livesdset;	
		if(select(livesdmax+1, &sec, NULL, NULL, NULL) < 0){
			printf("ERROR Select\n");
			exit(1);
		}
		/* look for messages from live clients */
		for (frsock = 3; frsock <= livesdmax; frsock++) {
			/* skip the listen socket */
			/* this case is covered separately */
			if (frsock == servsock)
				continue;

		 //	printf("frsock = %d\n", frsock);	

			if (FD_ISSET(frsock, &sec)) {
				char * clienthost; /* host name of the client */
				ushort clientport; /* port number of the client */
				
				/*
				 FILL HERE:			X
				 figure out client's host name and port
				 using getpeername() and gethostbyaddr()
				 */
				 printf("before getpeername\n");
		
				struct sockaddr_in addr;
				int sadd = sizeof(addr);
				if(getpeername(frsock, (struct sockaddr*)&addr,&sadd) < 0){						 printf("ERROR peername\n");
     		 	exit(2);
				}

				hp = gethostbyaddr(&addr.sin_addr, sizeof(addr.sin_addr), AF_INET);
				memcpy(&addr.sin_addr.s_addr, hp->h_addr, hp->h_length);
				clienthost =  hp->h_name;
				clientport = ntohs(serveraddr.sin_port);

				if(hp == NULL){
					printf("ERROR hp");
					exit(3);
				}

				/* read the message */
				char *msg = recvtext(frsock);
				if(!msg) {
					/* disconnect from client */
					printf("admin: disconnect from '%s(%hu)'\n", clienthost,
							clientport);

				/*
					 FILL HERE:			X
					 remove this guy from the set of live clients
					 */
					FD_CLR(frsock, &livesdset);

					/* close the socket */
					close(frsock);
				} else {
					/*
					 FILL HERE 
					 send the message to all live clients
					 except the one that sent the message
					 */
					//printf("readsomething");
					int i;
					for(i = 3; i <=livesdmax; i++){	
						if(i != frsock && i != servsock){
							sendtext(i, msg);

						}
					}
					char *gname, *mname;
								
	  			/* take action based on messge type */
	      			gname = msg;
	      			mname = gname + strlen(gname) + 1;
		
					/* display the message */
					printf("%s(%hu): %s", clienthost, clientport, msg);

					/* free the message */
					free(msg);
				}
			FD_CLR(frsock, &sec);
			}
		}
	
		/* look for connect requests */
		if (FD_ISSET (servsock, &sec) /* FILL HERE X: connect request from a new client? */ ) {

			/*
			 FILL HERE: 		X
			 accept a new connection request
			 */
			int csd;
			addr_size = sizeof(clientaddr);
			csd = accept(servsock, (struct sockaddr *)&clientaddr, &addr_size);
			memset(&serveraddr, 0, sizeof(serveraddr));

			/* if accept is fine? */
			if (csd != -1) {
				char * clienthost; /* host name of the client */
				ushort clientport; /* port number of the client */

				/*
				 FILL HERE:    X
				 figure out client's host name and port
				 using gethostbyaddr() and without using getpeername().
				 */
				//use clientaddr to get clients ip address;
					clienthost = (char*)malloc(sizeof(char) * 256);
					struct hostent *h;
					h  = gethostbyaddr(&clientaddr,  sizeof(clientaddr) , AF_INET);
						
					memcpy(&clientaddr.sin_addr.s_addr, h->h_addr, h->h_length);
					memcpy(clienthost, h->h_name, strlen(h->h_name)+1);
					clientport = ntohs(clientaddr.sin_port);
					
				
					printf("admin: connect from '%s' at '%hu'\n", clienthost,	clientport);

				/*
				 FILL HERE:
				 add this guy to set of live clients
				 */
					FD_SET(csd, &livesdset);

					if (csd > livesdmax)livesdmax = csd;
			} else {
				perror("accept");
				exit(0);
			}
			FD_CLR(servsock, &sec);
		}
	}
	close(servsock);
	return 0;
}
/*--------------------------------------------------------------------*/

