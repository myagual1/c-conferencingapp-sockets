/*--------------------------------------------------------------------*/
/* functions to connect clients and server */

#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netdb.h>
#include <time.h> 
#include <errno.h>

#include <stdlib.h>

#define MAXNAMELEN 256
/*--------------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/* prepare server to accept requests
 returns file descriptor of socket
 returns -1 on error
 */
int startserver() {
	int sd; /* socket descriptor */

	char * servhost; /* full name of this host */
	ushort servport; /* port assigned to this server */

	/*
	 FILL HERE        X
	 create a TCP socket using socket()
	 */
		sd = socket(AF_INET, SOCK_STREAM, 0);
		if (sd < 0){
    	printf("ERROR opening socket\n");
			exit(1);
		}
	/*
	 FILL HERE        X
	 bind the socket to some port using bind()
	 let the system choose a port
	 */
		struct sockaddr_in serv_addr;
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(0);
		serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		
		if(bind(sd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
			printf("ERROR binding\n");
			exit(2);
		}
	/* we are ready to receive connections */
	listen(sd, 5);

	/*
	 FILL HERE 				X
	 figure out the full host name (servhost)
	 use gethostname() and gethostbyname()
	 full host name is remote**.cs.binghamton.edu
	 */
	servhost = (char*)malloc(sizeof(char) * MAXNAMELEN);
	if(gethostname(servhost, sizeof(servhost)*MAXNAMELEN) < 0){
			printf("ERROR getting server name\n");
      exit(3);
	} 

	struct hostent *na;
	na =  gethostbyname(servhost);
  memcpy(&serv_addr.sin_addr.s_addr, na->h_addr, na->h_length);

	memcpy(servhost, na->h_name, strlen(na->h_name)+1);
//	printf("My hostname: %s\n", servhost);
	/*
	 FILL HERE				X
	 figure out the port assigned to this server (servport)
	 use getsockname()
	 */
	int slen = sizeof(serv_addr);	
	if(getsockname(sd, (struct sockaddr *) &serv_addr, &slen) == -1){
			printf("getsockname() failed\n");
      exit(4);
	}
	
	servport = ntohs(serv_addr.sin_port);
	/* ready to accept requests */
	printf("admin: started server on '%s' at '%hu'\n", servhost, servport);
	free(servhost);
	return (sd);
}
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
/*
 establishes connection with the server
 returns file descriptor of socket
 returns -1 on error
 */
int hooktoserver(char *servhost, ushort servport) {
	int sd; /* socket descriptor */

	ushort clientport; /* port assigned to this client */

	/*
	 FILL HERE              X
	 create a TCP socket using socket()
	 */
		 sd = socket(AF_INET, SOCK_STREAM, 0);
    if (sd < 0){
      printf("ERROR opening socket\n");
      exit(1);
    }
		
	/*
	 FILL HERE
	 connect to the server on 'servhost' at 'servport'
	 use gethostbyname() and connect()
	 */
	//const char *name;	
	struct hostent *h;

	struct sockaddr_in sa;
	sa.sin_family = AF_INET;
	sa.sin_port = htons(servport);

	h = gethostbyname(servhost);
	memcpy(&sa.sin_addr.s_addr, h->h_addr, h->h_length);

	if(connect(sd,(struct sockaddr *)&sa, sizeof(sa)) < 0){
		 printf("ERROR connecting\n");
      exit(1);
	}
	/*
	 FILL HERE 			X
	 figure out the port assigned to this client
	 use getsockname()
	 */

	struct sockaddr_in client;
	int l = sizeof(client);
	getsockname(sd, (struct sockaddr *)&client, &l);
	
	clientport = ntohs(client.sin_port);

	printf("worked\n");
	/* succesful. return socket descriptor */
	printf("admin: connected to server on '%s' at '%hu' thru '%hu'\n", servhost,
			servport, clientport);
	printf(">");
	fflush(stdout);
	return (sd);
}
/*----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
int readn(int sd, char *buf, int n) {
	int toberead;
	char * ptr;

	toberead = n;
	ptr = buf;
	while (toberead > 0) {
		int byteread;

		byteread = read(sd, ptr, toberead);
		if (byteread <= 0) {
			if (byteread == -1)
				perror("read");
			return (0);
		}

		toberead -= byteread;
		ptr += byteread;
	}
	return (1);
}

char *recvtext(int sd) {
	char *msg;
	long len;

	/* read the message length */
	if (!readn(sd, (char *) &len, sizeof(len))) {
		return (NULL);
	}
	len = ntohl(len);

	/* allocate space for message text */
	msg = NULL;
	if (len > 0) {
		msg = (char *) malloc(len);
		if (!msg) {
			fprintf(stderr, "error : unable to malloc\n");
			return (NULL);
		}

		/* read the message text */
		if (!readn(sd, msg, len)) {
			free(msg);
			return (NULL);
		}
	}

	/* done reading */
	return (msg);
}

int sendtext(int sd, char *msg) {
	long len;

	/* write lent */
	len = (msg ? strlen(msg) + 1 : 0);
	len = htonl(len);
	write(sd, (char *) &len, sizeof(len));

	/* write message text */
	len = ntohl(len);
	if (len > 0)
		write(sd, msg, len);
	return (1);
}
/*----------------------------------------------------------------*/

